# FSFE Affiliate Links

This userscript sets affiliate referers automatically to support the FSFE. See [this site](https://wiki.fsfe.org/Activities/SupportPrograms) for more information on the FSFE's support programmes.

## Usage

1. Install an add-on in your browser to enable you using custom scripts
  - Firefox: [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/), [Violentmonkey](https://addons.mozilla.org/firefox/addon/violentmonkey)
  - Chromium/Chrome: [Violentmonkey](https://chrome.google.com/webstore/detail/violent-monkey/jinjaccalgkegednnccohejagnlnfdag)
  - Opera: [Tampermonkey](https://addons.opera.com/en/extensions/details/tampermonkey-beta/)
  - Safari: [Tampermonkey](http://tampermonkey.net/?browser=safari)
2. Install the user script from [GreasyFork](https://greasyfork.org/en/scripts/374701-fsfe-affiliate-programs-extras-auto-ssl).
3. Support the FSFE when buying online on the supported platforms if you decide to use them.

## Authors

The script has originally been created by [Hannes Hauswedell](https://hannes.hauswedell.net/). It has been mirrored to a public repository and slightly adapted by the FSFE.
